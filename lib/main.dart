import 'package:flutter/material.dart';
import 'package:learn_flutter_1/quiz.dart';
import 'package:learn_flutter_1/result.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return MyAppState();
  }
}

class MyAppState extends State<MyApp> {
  var _questionIndex = 0;
  var _totalScore = 0;
  final _questions = const [
    {
      'questionText': 'What\'s your favorite color?',
      'answers': [
        {'text': 'red', 'score': 10},
        {'text': 'green', 'score': 5},
        {'text': 'blue', 'score': 3},
        {'text': 'yellow', 'score': 1}
      ]
    },
    {
      'questionText': 'What\'s your favorite animal?',
      'answers': [
        {'text': 'lion', 'score': 10},
        {'text': 'zebra', 'score': 5},
        {'text': 'horse', 'score': 3},
        {'text': 'deer', 'score': 1}
      ]
    },
    {
      'questionText': 'What\'s your favorite fast food?',
      'answers': [
        {'text': 'hamburger', 'score': 10},
        {'text': 'sandwich', 'score': 5},
        {'text': 'pizza', 'score': 3},
        {'text': 'fri', 'score': 1}
      ]
    }
  ];

  void _answerQuestion(int score) {
    _totalScore += score;
    setState(() {
      _questionIndex++;
    });
  }

  void _resetQuiz() {
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Quiz App"),
        ),
        body: _questionIndex < _questions.length
            ? Quiz(
                questions: _questions,
                questionIndex: _questionIndex,
                answerQuestion: _answerQuestion,
              )
            : Result(
                totalScore: _totalScore,
                resetHandler: _resetQuiz,
              ),
      ),
    );
  }
}
